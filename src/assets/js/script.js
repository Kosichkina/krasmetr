@@include('../components/jquery/dist/jquery.js')
@@include('../components/tether/dist/js/tether.min.js')
@@include('../components/bootstrap/dist/js/bootstrap.min.js')
@@include('../components/bootstrap-select/dist/js/bootstrap-select.js')
@@include('../components/slick-carousel/slick/slick.js')
@@include('../components/nouislider/distribute/nouislider.js')

$(window).on('load', function() {
    $('.main-slider-slick').slick({
        arrows: false,
        dots: true
    });


    $('.object-gallery-slider').slick({
        arrows: false,
        dots: false,
        asNavFor: '.object-gallery-nav'
    });


    $('.object-gallery-nav').slick({
        arrows: false,
        dots: false,
        slidesToShow: 7,
        slidesToScroll: 1,
        asNavFor: '.object-gallery-slider',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    arrows: false
                }
            }
        ]
    });

    $('.advice-slider').slick({
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });

    if (document.getElementById('catalog-filter-slider-1')) {
        var slider = document.getElementById('catalog-filter-slider-1');

        noUiSlider.create(slider, {
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

    if(document.getElementById('catalog-filter-slider-2')) {
        var slider2 = document.getElementById('catalog-filter-slider-2');

        noUiSlider.create(slider2, {
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

    if(document.getElementById('catalog-filter-slider-3')) {
        var slider3 = document.getElementById('catalog-filter-slider-3');

        noUiSlider.create(slider3, {
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }
    if(document.getElementById('catalog-filter-slider-4')) {
        var slider4 = document.getElementById('catalog-filter-slider-4');

        noUiSlider.create(slider4, {
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }


    if(document.getElementById('calculator-options-1')) {
        noUiSlider.create(document.getElementById('calculator-options-1'), {
            start: [ 4000 ],
            range: {
                'min': [  2000 ],
                'max': [ 10000 ]
            }
        });
    }
    if(document.getElementById('calculator-options-2')) {
        noUiSlider.create(document.getElementById('calculator-options-2'), {
            start: [ 4000 ],
            range: {
                'min': [  2000 ],
                'max': [ 10000 ]
            }
        });
    }
    if(document.getElementById('calculator-options-3')) {
        noUiSlider.create(document.getElementById('calculator-options-3'), {
            start: [ 4000 ],
            range: {
                'min': [  2000 ],
                'max': [ 10000 ]
            }
        });
    }
    if(document.getElementById('calculator-options-4')) {
        noUiSlider.create(document.getElementById('calculator-options-4'), {
            start: [ 4000 ],
            range: {
                'min': [  2000 ],
                'max': [ 10000 ]
            }
        });
    }

    $('.product-favorite').on('click', function(e){
        e.preventDefault();
        $(this).closest('.product').toggleClass('favorite');
    });

    $('.catalog-filter-block-title').on('click', function(){
        $(this).closest('.catalog-filter-block-item').find('.catalog-filter-content').slideToggle();
    });

    $('.filter-link-m a').on('click', function(){
        $('.modal-bg').fadeToggle();
        $('.catalog-filter').fadeToggle();
    });

    $('.modal-bg').on('click', function(){
        $('.modal-bg').fadeOut();
        $('.catalog-filter').fadeOut();
    });

    $('.header-trigger').on('click', function(){
        $('.header-nav').slideToggle();
    })
});